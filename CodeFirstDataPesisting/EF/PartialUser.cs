﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstDataPesisting.EF
{
    public partial class User
    {
        public override string ToString()
        {
            return $"Name: {this.Name} Email: {this.Email}";
        }
    }
}
