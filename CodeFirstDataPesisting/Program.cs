﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirstDataPesisting.EF;

namespace CodeFirstDataPesisting
{
    class Program
    {
        static void Main(string[] args)
        {
            var Code = new CodeFirstData();
            Code.RunProgram();
            Console.ReadLine();
        }
       
    }
}
