﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeFirstMigration.Models
{
    public partial class MigrationEntity : DbContext
    {
        public MigrationEntity()
            : base("name=MigrationsEF")
        {
        }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.Balance)
                .HasPrecision(38, 2);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.Amount)
                .HasPrecision(38, 2);
        }
    }
}
