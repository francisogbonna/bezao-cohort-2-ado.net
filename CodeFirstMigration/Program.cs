﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirstMigration.EF;
using CodeFirstMigration.Models;
using System.Data.Entity;

namespace CodeFirstMigration
{
    class Program
    {
        static void Main(string[] args)
        {
            //Database.SetInitializer(new MyDataInitializer());  //commented out so it doesn't drop and recreate the DB
            Console.WriteLine($"Working with Code First ADO.NET Entity Framework");
            try
            {
                using (var context = new MigrationEntity())
                {
                    Console.WriteLine("Hello");
                    foreach (User u in context.Users)
                    {
                        Console.WriteLine($"{u.Name} {u.Email}");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException.Message);
            }
            Console.ReadLine();
        }
    }
}
