﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BEZAOPayDAL.Models;

namespace BEZAOPayDAL
{
    public class BEZAODAL
    {
        private readonly string _connectionString;

        public BEZAODAL() :
            this(@"Data Source=POPEFRANCIS;Initial Catalog=BEZAOPay;Integrated Security=True")
        {

        }

        public BEZAODAL(string connectionString)
        {
            _connectionString = connectionString;
        }


        private SqlConnection _sqlConnection = null;
        private void OpenConnection()
        {
            _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
            _sqlConnection.Open();
        }

        private void CloseConnection()
        {
            if (_sqlConnection?.State != ConnectionState.Closed)
                _sqlConnection?.Close();
        }
        private void ErrorMessage(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\nOops! You encountered {msg} error trying to interact with the Database.");
            Console.ResetColor();
            return;
        }


        public IEnumerable<User> GetAllUsers()
        {
            try
            {
                OpenConnection();
            }
            catch (Exception)
            {
                ErrorMessage("Connection");
            }

            var users = new List<User>();

            using (var command = new SqlCommand("SpGetAllUsers", _sqlConnection))
            {
                command.CommandType = CommandType.StoredProcedure;
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    users.Add(new User
                    {
                        Id = (int)reader["Id"],
                        Name = (string)reader["Name"],
                        Email = (string)reader["Email"]


                    });
                }
                reader.Close();
            }
            return users;
        }
        //Get a single user from the database
        public User GetSingleUser(int id)
        {
            string query = $"select * from Users where id={id}";
            User user = null;
            try
            {
                OpenConnection();
                using (SqlCommand command = new SqlCommand(query, _sqlConnection))
                {
                    command.CommandType = CommandType.Text;
                    var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        user = new User
                        {
                            Id = (int)reader["Id"],
                            Name = (string)reader["Name"],
                            Email = (string)reader["Email"]
                        };
                    }
                    reader.Close();
                }
            }
            catch (Exception)
            {
                ErrorMessage("Connection");
            }
            return user;
        }
        //Get user using the stored procedure
        public User GetStoredUserName(int id)
        {
            User user = new User();
            try
            {
                OpenConnection();
                using(SqlCommand command = new SqlCommand("GetName",_sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter idParameter = new SqlParameter()
                    {
                        ParameterName="@Id",
                        SqlDbType=SqlDbType.Int,
                        Value=id,
                        Direction=ParameterDirection.Input
                    };
                    SqlParameter nameParameter = new SqlParameter()
                    {
                        ParameterName = "@name",
                        SqlDbType = SqlDbType.Char,
                        Size = 50,
                        Direction = ParameterDirection.Output
                    };
                    SqlParameter mailParameter = new SqlParameter()
                    {
                        ParameterName = "@email",
                        SqlDbType = SqlDbType.VarChar,
                        Size = 50,
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(idParameter);
                    command.Parameters.Add(nameParameter);
                    command.Parameters.Add(mailParameter);

                    command.ExecuteNonQuery();
                    try
                    {
                        user.Name = (string)command.Parameters["@name"].Value;
                        user.Email = (string)command.Parameters["@email"].Value;
                        user.Id = id;
                    }
                    catch (Exception)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"User not found!");
                        Console.ResetColor();
                    }
                    CloseConnection();
                }
            }
            catch (Exception e)
            {
                ErrorMessage("Connection");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{e.Message}");
                Console.ResetColor();
            }
            return user;
        }

        public UserDetails GetUserInfo(int id)
        {
            UserDetails details = new UserDetails();
            try
            {
                OpenConnection();
                using(SqlCommand command = new SqlCommand("SpGetUserDetails", _sqlConnection))
                {
                    SqlParameter acParam = new SqlParameter()
                    {
                        ParameterName = "@acNo",
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Output
                    };
                    SqlParameter nameParam = new SqlParameter()
                    {
                        ParameterName = "@name",
                        Size = 50,
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Output
                    };
                    SqlParameter emailParam = new SqlParameter()
                    {
                        ParameterName = "@email",
                        Size = 40,
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Output
                    };
                    SqlParameter balanceParam = new SqlParameter()
                    {
                        ParameterName = "@bal",
                        SqlDbType = SqlDbType.Decimal,
                        Direction = ParameterDirection.Output
                    };
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(acParam);
                    command.Parameters.Add(nameParam);
                    command.Parameters.Add(balanceParam);
                    command.Parameters.Add(emailParam);
                    command.Parameters.AddWithValue("@Id", id);

                    command.ExecuteNonQuery();
                    try
                    {
                        details.Name = (string)command.Parameters["@name"].Value;
                        details.Email = (string)command.Parameters["@email"].Value;
                        details.AccountNumber = (int)command.Parameters["@acNo"].Value;
                        details.Balance = (decimal)command.Parameters["@bal"].Value;
                        details.Id = (int)command.Parameters["@Id"].Value;
                    }
                    catch (Exception)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Can't get user details.");
                        Console.ResetColor();
                    }
                   
                }
                
            }
            catch (Exception)
            {
                ErrorMessage("Connect");
                
            }
            return details;
        }
        public string DeleteUser(int id)
        {
            string msg = null;
            try
            {
                OpenConnection();
                using(SqlCommand command = new SqlCommand("DeleteUser", _sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    
                    command.Parameters.AddWithValue("@id",id);
                    try
                    {
                        command.ExecuteNonQuery();
                        msg = "User has been deleted.";
                    }
                    catch (Exception )
                    {
                        msg = "User not deleted.";
                    }
                    CloseConnection();
                }
            }
            catch (Exception)
            {
                ErrorMessage("Connection");
                msg = "User does not exist.";
            }
            return msg;
        }
        public string InsertTransaction()
        {
            string msg = null;
            Console.WriteLine("Enter username:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter Email:");
            var mail = Console.ReadLine();
            var acNo = GenerateAccountNo();
            decimal bal = 20_000;
            Console.WriteLine($"Your account number is: {acNo}");

            OpenConnection();
            SqlTransaction transaction = _sqlConnection.BeginTransaction();
            try
            {
                SqlCommand userCommand = new SqlCommand("AddUser", _sqlConnection, transaction);
                SqlCommand accountCommand = new SqlCommand("AddUserAccount", _sqlConnection, transaction);
                Console.WriteLine("transaction area");
                userCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter idParam = new SqlParameter()
                {
                    ParameterName = "@userId",
                    SqlDbType=SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };
                SqlParameter nameParam = new SqlParameter()
                {
                    ParameterName = "@name",
                    Size = 50,
                    SqlDbType = SqlDbType.NVarChar,
                    Value = name
                };
                SqlParameter emailParam = new SqlParameter()
                {
                    ParameterName = "@email",
                    Size = 50,
                    SqlDbType = SqlDbType.NVarChar,
                    Value = mail
                };
                userCommand.Parameters.Add(emailParam);
                userCommand.Parameters.Add(nameParam);
                userCommand.Parameters.Add(idParam);
                userCommand.ExecuteNonQuery();
                var userId = userCommand.Parameters["@userId"].Value;
                Console.WriteLine(userId);

                accountCommand.CommandType = CommandType.StoredProcedure;
                accountCommand.Parameters.AddWithValue("@acNo", acNo);
                accountCommand.Parameters.AddWithValue("@bal", bal);
                accountCommand.Parameters.AddWithValue("@userId", userId);
                accountCommand.ExecuteNonQuery();

                transaction.Commit();
                msg = "Transaction was successful!";
            }
            catch (SqlException )
            {
                transaction.Rollback();
                msg = "Oops! Error: Transaction aborted.";
            }
            finally
            {
                CloseConnection();
            }
            return msg;
        }
        public void MakeTransaction(string user)
        {
            string query = "select @id=Id from Users where Name =@userName";
            OpenConnection();
            SqlTransaction transaction = _sqlConnection.BeginTransaction();
            SqlCommand userCommand = new SqlCommand(query, _sqlConnection);
            userCommand.Parameters.AddWithValue("@userName", user.Trim());
            userCommand.Parameters.Add("@id", SqlDbType.Int);
            userCommand.ExecuteNonQuery();
            var id = userCommand.Parameters["@id"].Value;
            Console.WriteLine($"the id is: {id}");

        }
        private int GenerateAccountNo()
        {
            Random rand = new Random();
            int acNo = rand.Next(100000000, 999999999);
            return acNo;
        }
    }
}
