﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAOPayDAL.Models
{
    public class UserDetails : Account
    {
        public string Name { get; set; }
        public string  Email { get; set; }
    }
}
