﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPayDAL;
using System.Configuration;


namespace BEZAOPay
{
    class Program
    {
        static void Main(string[] args)
        {
            var conString = ConfigurationManager.ConnectionStrings["BEZAOConnect"].ConnectionString;
            var db = new BEZAODAL(conString);

            
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\t\tWelcome to BezaoPay! \n\tLet's take care of the payment for you...");
            Console.ResetColor();
                
            StringBuilder menu = new StringBuilder();
            menu.Append("Choose your prefered operations:");
            menu.AppendLine("\n1. Register.\n2. View premium users.\n3. Perform transaction.\n4. View specific user.\n5. Delete a User.\n6. View a Transaction\n7. Delete a Transaction");
            menu_section:
            Console.WriteLine(menu);
            var option = Console.ReadLine();
            if (string.IsNullOrEmpty(option))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: Invalid option selected.");
                Console.ResetColor();
                goto menu_section;
            }
            switch (option)
            {
                case "1":
                    db.InsertTransaction();
                    var next = NextOperation();
                    if (next == "yes")
                    {
                        Console.Clear();
                        goto menu_section;
                    }
                    else
                    {
                        Console.WriteLine("Thanks for using BEZAOPay App!");
                    }
                    break;
                case "2":
                    Console.WriteLine("Loading users please waite...");
                    var users = db.GetAllUsers();
                    foreach (var user in users)
                    {
                        Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
                    }
                    var more = NextOperation();
                    if (more == "yes")
                    {
                        Console.Clear();
                        goto menu_section;
                    }
                    else
                    {
                        Console.WriteLine("Thanks for using BEZAOPay App!");
                    }

                    break;
                case "3":
                    //perform transaction
                    Console.WriteLine("Select Transaction type:\n1. Credit transaction.\n2. Debit Transaction");
                    var response = Console.ReadLine();
                    if(response != "1" && response != "2")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: Invalid response.");
                        Console.ResetColor();
                        goto menu_section;
                    }
                    //if(response == "1")
                    //{
                    //    Console.WriteLine("Enter your Username:");
                    //    var user = Console.ReadLine();
                    //    Console.WriteLine("Enter Amount:");
                    //    //try
                    //    //{
                    //        int amount =int.Parse( Console.ReadLine());
                    //        db.MakeTransaction(user);
                    //    //}
                    //    //catch (Exception )
                    //    //{
                    //    //    Console.ForegroundColor = ConsoleColor.Red;
                    //    //    Console.WriteLine("Error: Invalid input");
                    //    //    Console.ResetColor();
                    //    //}
                    //}
                    break;
                case "4":
                    //view perticular user
                    Console.WriteLine("Enter user ID:");
                    int id;
                    if(!int.TryParse(Console.ReadLine(), out id))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: Expected a digital value.");
                        Console.ResetColor();
                        goto menu_section;
                    }
                    var details = db.GetUserInfo(id);
                    if (!string.IsNullOrEmpty(details.Email) && !string.IsNullOrEmpty(details.Name))
                    {
                        Console.WriteLine("\nUser details: ");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"user ID:{details.Id}\nName: {details.Name}\nEmail: {details.Email}\nAccountNo: {details.AccountNumber}\nBalance: {details.Balance.ToString("N")}");
                        Console.ResetColor();
                    }
                    var tryAgain = NextOperation();
                    if (tryAgain == "yes")
                    {
                        Console.Clear();
                        goto menu_section;
                    }
                    else
                    {
                        Console.WriteLine("Thanks for using BEZAOPay App!");
                    }
                    break;
                case "5":
                    //delete a user
                    Console.WriteLine("Enter user ID:");
                    int userId;
                    if (!int.TryParse(Console.ReadLine(), out userId))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: Expected a digital value.");
                        Console.ResetColor();
                        goto menu_section;
                    }
                    var Deleteuser = db.DeleteUser(userId);
                    Console.WriteLine(Deleteuser);
                    var repeatActions = NextOperation();
                    if (repeatActions == "yes")
                    {
                        Console.Clear();
                        goto menu_section;
                    }else
                    {
                        Console.WriteLine("Thanks for using BEZAOPay App!");
                    }
                    break;
                case "6":
                    //view transaction
                    break;
                case "7":
                    //delete a transaction
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: Invalid input.");
                    Console.ResetColor();
                    goto menu_section;
            }  
          
            Console.ReadLine();
        }
        private static string NextOperation()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Do you want to perform another operation?\n1. Yes\n2. No");
            Console.ResetColor();
            var answer = Console.ReadLine();
            if(answer.ToLower().Equals("yes") || answer.Equals("1"))
            {
                
                return "yes";
            }
            return "no";
        }
    }
}
