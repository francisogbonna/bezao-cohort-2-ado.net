
CREATE TABLE [dbo].[Users]
(
[Id] INT IDENTITY(1, 1) NOT NULL ,
[Name] NVARCHAR(50) NOT NULL ,
[Email] NVARCHAR(50) NOT NULL ,

PRIMARY KEY CLUSTERED ( [Id] ASC )
);

CREATE TABLE [dbo].[Accounts]
(
[Id] INT IDENTITY(1, 1) NOT NULL ,
[UserId] INT  NOT NULL ,
[Account_Number] INT NOT NULL ,
[Balance] Decimal(38,2) NOT NULL ,

PRIMARY KEY CLUSTERED ( [Id] ASC ),
CONSTRAINT [FK_Accounts_Users] 
   FOREIGN KEY (UserId) REFERENCES [Users]([Id])
      ON DELETE CASCADE
);

CREATE TABLE [dbo].[Transactions]
(
[Id] INT IDENTITY(1, 1) NOT NULL ,
[UserId] INT  NOT NULL ,
[Mode] NVARCHAR(10) NOT NULL ,
[Amount] Decimal(38,2) NOT NULL ,
[Time] DATETIME NOT NULL,

PRIMARY KEY CLUSTERED ( [Id] ASC ),
CONSTRAINT [FK_Transactions_Users] 
   FOREIGN KEY (UserId) REFERENCES [Users]([Id])
      ON DELETE CASCADE
);

Go
CREATE PROCEDURE [AddTransaction]
	@userId int,
	@amount decimal(38,2),
	@mode nvarchar(20),
	@time datetime
AS
	insert into Transactions (UserId,Mode,Amount,Time) values(@userId,@mode,@amount,@time);
Go
CREATE PROCEDURE [dbo].[AddUser]
	@name nvarchar(50),
	@email nvarchar(50),
	@userId int output

AS
	Insert into Users (Name, Email) values(@name,@email) set @userId =IDENT_CURRENT('Users')
	return @userId;
Go
CREATE PROCEDURE [dbo].[AddUserAccount]
	@acNo int,
	@userId int,
	@bal decimal(38,2)

AS
	Insert into Accounts(UserId,Account_Number,Balance) values(@userId,@acNo,@bal);
Go
CREATE PROCEDURE GetName
@Id int,
@name char(50) output
AS
SELECT @name = Name from Users where Id = @Id
Go
CREATE PROCEDURE [dbo].[DeleteUser]
	@id int
AS
	Delete from Users where Id=@id;
Go
CREATE PROCEDURE [dbo].[GetAccount]
	@userId int,
	@accountNo int output,
	@accountBal decimal(32,2) output
AS
	SELECT @accountNo = Account_Number,@accountBal = Balance from Accounts where UserId=@userId;
Go
CREATE PROCEDURE [dbo].[GetAllUsers]
AS
	SELECT * from Users;
Go
CREATE PROCEDURE GetName
	@Id int,
	@name char(50) output,
	@email varchar(50) output
AS
SELECT  @name=Name,@email= Email from Users where Id = @Id;
Go
CREATE PROCEDURE [GetTransactionDetails]
	@userId int
	
AS
	select Mode, Amount, Time from Transactions where UserId = @userId;
Go
CREATE PROCEDURE [dbo].[SpGetAllAccounts]

AS
	SELECT * from Accounts;
Go
CREATE PROCEDURE [dbo].[SpGetAllTransactions]

AS
	SELECT * from Transactions;
Go
CREATE PROCEDURE [dbo].[SpGetAllUsers]

AS
	SELECT * from Users;
Go
CREATE PROCEDURE [dbo].[SpGetUserDetails]
	@Id int,
	@acNo int output, 
	@bal decimal(38,2) output,
	@name varchar(50) output,
	@email varchar(40) output
AS
	SELECT @name =u.Name,@email = u.Email, @acNo =a.Account_Number, @bal =a.Balance from Users u, Accounts a where u.Id =@id and u.Id = a.UserId;
	Go
	CREATE PROCEDURE [UpdateAccount]
	@userId int,
	@amount decimal(38,2)
AS
	update Accounts set Balance=@amount where UserId = @userId;
	Go

INSERT INTO Users (Name, Email) VALUES ('Tochukwu', 'tochukwu@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Shola', 'shola.nejo@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Chikky', 'chikky@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Dara', 'dara@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Alex', 'alex@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Uriel', 'Uriel@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Kachi', 'kachi@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Chinedu', 'chinedu@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Loveth', 'loveth@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Sunday', 'sunday@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Sammy', 'sammy@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Einstien', 'einstien@domain.com')
INSERT INTO Users (Name, Email) VALUES ('KCM', 'kcm@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Obinna', 'obi@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Gideon', 'Giddy@domain.com')
INSERT INTO Users (Name, Email) VALUES ('Francis', 'SorrySir@domain.com')
 

 
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (1,0293842983,1008900.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (2,0293842653,1008700.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (3,0293840983,1000900.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (4,0293847683,1008700.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (5,0293845683,1006700.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (6,0293844283,1670000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (7,0293982983,1340000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (8,0293112983,1002300.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (9,0293672983,1009800.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (10,0290942983,1067000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (11,0293898983,1076000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (12,0293887983,1087000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (13,0293800983,1009000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (14,0293812983,1087000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (15,0293854983,1034000.99)
INSERT INTO Accounts (UserId, Account_Number, Balance) VALUES (16,0290954983,1034670.99)


