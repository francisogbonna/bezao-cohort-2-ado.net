﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CodeFirstMigration.Models;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace CodeFirstMigration.EF
{
    public class MyDataInitializer : DropCreateDatabaseAlways<MigrationEntity>
    {
        protected override void Seed(MigrationEntity context)
        {
            base.Seed(context);
            var Addusers = new List<User>()
            {
                new User{Name="Pope Francis",Email="pope@gmail.com"},
                new User{Name="Obinna",Email="obicubana@gmail.com"},
                new User{Name="Sammy",Email="sammy@yahoo.com"}
            };
            context.Users.AddOrUpdate(u => new { u.Name, u.Email },Addusers.ToArray());
            var accounts = new List<Account>()
            {
                new Account{UserId =1,Account_Number=1234567283, Balance= 500000.00m},
                new Account{UserId =2,Account_Number=1234567283, Balance= 500000.00m},
                new Account{UserId=3,Account_Number=1234567283, Balance= 500000.00m}
            };
            context.Accounts.AddOrUpdate(acc => new { acc.UserId, acc.Account_Number, acc.Balance }, accounts.ToArray());
        }
    }
}
