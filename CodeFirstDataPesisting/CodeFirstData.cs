﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirstDataPesisting.EF;

namespace CodeFirstDataPesisting
{
    public class CodeFirstData
    {
        public void RunProgram()
        {
            Console.WriteLine("Welcome to BEZAOPay  using Code First");
            Console.WriteLine("Select prefered operation:");
            Console.WriteLine("1. View Users\n2. Add new user\n3. Get user details\n4. Delete user\n5. Update username");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    GetUsersDetails();
                    break;
                case "2":
                    AddNewUser();
                    break;
                case "3":
                    Console.WriteLine("Enter user Id");
                    var id = int.Parse(Console.ReadLine());
                    GetUsers(id);
                    break;
                case "4":
                    Console.WriteLine("Enter user ID:");
                    var uid = int.Parse(Console.ReadLine());
                    DeleteUser(uid);
                    break;
                case "5":
                    Console.WriteLine("Enter user ID:");
                    var userId = int.Parse(Console.ReadLine());
                    Console.WriteLine("Enter New Name for user:");
                    var name = Console.ReadLine();
                    UpdateUser(userId, name);
                    break;
                default:
                    break;
            }

        }
        private void GetUsers(int id)
        {
            using (EntityDM EfContext = new EntityDM())
            {
                foreach (var user in EfContext.Users.SqlQuery($"select * from Users where Id ={id}"))
                {
                    Console.WriteLine(user.ToString());
                }
            }
        }
        private int AddNewUser()
        {
            Console.WriteLine("Enter username:");
            var name = Console.ReadLine();
            Console.WriteLine("Enter Email Address:");
            var email = Console.ReadLine();


            using (EntityDM entityDM = new EntityDM())
            {
                var user = new User { Name = name, Email = email };
                try
                {
                    entityDM.Users.Add(user);
                    entityDM.SaveChanges();
                    return user.Id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException.Message);
                    return 0;
                }

            }
        }
        private void GetAllusers()
        {
            using (EntityDM entityDM = new EntityDM())
            {
                try
                {
                    foreach (var user in entityDM.Users)
                    {
                        Console.WriteLine($"{user.Id} {user}");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException.Message);
                }
            }
        }
        private void GetUsersDetails()
        {
            try
            {
                using (EntityDM entityDM = new EntityDM())
                {
                    //foreach (var user in entityDM.Users)
                    //{
                    //    foreach (var acc in user.Accounts)
                    //    {
                    //        Console.WriteLine($"{user.Name} {user.Email} {acc.Account_Number} {acc.Balance}");
                    //    }
                    //}
                    entityDM.Configuration.LazyLoadingEnabled = false;
                    foreach (User user in entityDM.Users)
                    {
                        entityDM.Entry(user).Collection(u => u.Accounts).Load();
                        foreach (var acc in user.Accounts)
                        {
                            Console.WriteLine($"{user.Id} {user.ToString()} Account Number: {acc.Account_Number} Available Balance: {acc.Balance}");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException.Message);
            }
        }
        private void DeleteUser(int id)
        {
            try
            {
                using (EntityDM entityDM = new EntityDM())
                {
                    //Search for user in DB
                    User userToDelete = entityDM.Users.Find(id);
                    if (userToDelete != null)
                    {
                        entityDM.Users.Remove(userToDelete);
                        //check the state of object
                        if (entityDM.Entry(userToDelete).State != System.Data.Entity.EntityState.Deleted)
                        {
                            throw new Exception("Unable to Delete User");
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($"{userToDelete.Name} has been Deleted from Record!");
                            Console.ResetColor();
                        }
                        entityDM.SaveChanges();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"User with ID: {id} does not exist.");
                        Console.ResetColor();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
         private void UpdateUser(int id, string name)
         {
            using (EntityDM entityDM = new EntityDM())
            {
                User userToUpdate = entityDM.Users.Find(id);
                if (userToUpdate != null)
                {
                    userToUpdate.Name = name;
                    entityDM.SaveChanges();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"User with ID: {id} not Found!");
                    Console.ResetColor();
                }
            }
         }
    }
}
